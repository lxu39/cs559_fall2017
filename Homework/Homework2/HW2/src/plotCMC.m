prob = load('verificationProb1.mat');
gallery = load('gallery.mat');
[rowProb, colProb] = size(prob.veri);
[rowGal, colGal] = size(gallery.matrix);
maps = load('sortedProb1CMC.mat');
maps = maps.maps;
% Draw CMC
[rowmap, colmap] = size(maps);
res = [];
for k = 1 : rowGal
    count = 0;
    for i = 1 : rowmap
        name = maps{i, 1};
        sortedMap = maps{i, 2};
        [row, col] = size(sortedMap);
        if k >= row
           count = count + 1;
           continue;
        end
        for j = 1 : k
            if strcmpi(sortedMap{j, 1}, name)
               count = count + 1;
               break;
            end
        end
    end
    count = count / rowmap;
    res = [res, count];
end
x = 1 : 1 : rowGal;
y = res;
plot(x, y);
xlabel('Rank');
ylabel('Recognition Rate');