% Histograms
h1 = histfit(hd_genuine, 40, 'normal');
hold on;
h2 = histfit(hd_imposter, 40, 'normal');

% Get the curves
hLines = findobj('Type','Line');
set(hLines(1),'Color','r')
set(hLines(2),'Color','g')

% delete Histograms
delete(h1(1))
delete(h2(1))

hPatches=findobj(gca,'Type','patch');

hold off;

% Put up legend.
legend1 = sprintf('Genuine');
legend2 = sprintf('Imposter');
legend({legend1, legend2});