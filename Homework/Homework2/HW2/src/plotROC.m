fileGenuine = load('genuineProb1.mat');
matrix = fileGenuine.genuine;
mapGenuine = containers.Map(0.0, 0);
[rowG, col] = size(matrix);

index = 0.00;
TMR = [];
FMR = [];
while index <= 1
    TMsum = 0;
    k = keys(mapGenuine);
    frequences = values(mapGenuine);
    for i = 1 : length(mapGenuine)
        if k{i} <= index
           TMsum = TMsum + frequences{i}; 
        end
    end
    TMsum = TMsum / rowG;
    TMR = [TMR TMsum];
    
    FMsum = 0;
    k = keys(mapImposter);
    frequences = values(mapImposter);
    for i = 1 : length(mapImposter)
        if k{i} <= index
           FMsum = FMsum + frequences{i}; 
        end
    end
    FMsum = FMsum / rowI;
    FMR = [FMR FMsum];
    
    index = index + 0.01;
end
% Draw ROC
plot(TMR, FMR);
xlabel('FMR');
ylabel('TMR');