g = 'C:\Users\CHEN\Desktop\HW2\LG2200-2008-03-11_13\'; % local path of the gallery
gallery = dir(g);
galleryTemplates = containers.Map;
galleryMasks = containers.Map;

p = 'C:\Users\CHEN\Desktop\HW2\LG2200-2010-04-27_29\';% local path of the probes
probes = dir(p);
probesTemplates = containers.Map;
probesMasks = containers.Map;

for i = 4: numel(gallery)
    name = gallery(i).name;
    [tmp, mask] = createiristemplate(name);
    id = strsplit(name, '.');
    id = id{1};
    galleryTemplates(id) = tmp;
    galleryMasks(id) = mask;
end

for i = 4: numel(probes)
    name = probes(i).name;
    [tmp, mask] = createiristemplate(name);
    id = strsplit(name, '.');
    id = id{1};
    probesTemplates(id) = tmp;
    probesMasks(id) = mask;
end

% compare and get hamming distance
for i = keys(probes)
    for j = keys(gallery)
        hd = gethammingdistance(probesTemplates(i), probesMasks(i), galleryTemplates(j), galleryMasks(j), 15);
        disp(name);
        disp(j);
        disp(hd);
    end
end
